import run_numbers
import config as c
import Process
import os
output_dir = os.path.join(c.root_path, "JO")
jo_pattern = "MC12.{run_number}.MadgraphPythia_CT10_P2011C_gg{mediator}tt{mass}_tb{tanb100:03d}_nonallhad_{no}interf.py"
for mediator in ("H", "A"):
		for mass, tanbs in zip((600, 700), ((0.4, 0.5, 0.9, 1.5, 3.5, 6.0), (0.4, 0.5, 0.7, 1.7, 3.0, 0.9, 1.5, 3.5))):
			for tanb in tanbs:
				for signal_type in ("S", "SI"):
					process = Process.ttbarResonanceProcess(**{"m"+mediator:mass, "tanb": tanb, "signal_type": signal_type})
					kwargs = dict(run_number = str(process).split(".")[3], 
						          mediator = mediator, 
						          mass = mass, 
						          tanb100 = int(tanb*100), 
						          tanb = tanb,
						          no = "no" if signal_type == "S" else "",
						          interference = "" if signal_type == "S" else "+interference")
					with open(os.path.join(output_dir, jo_pattern).format(**kwargs), "w") as of:
						with open("jo_template.py", "r") as temp:
							of.write(temp.read().format(**kwargs))