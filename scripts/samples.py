import glob, os
import main
import config as c
process = "206769to206768"
run_number, run_number_to = process.split("to")

folder = os.path.abspath(glob.glob("./run/output_20*to20*/*." + run_number + ".*/")[0])
c.output_path = folder.rsplit("/", 1)[0]
c.work_path = os.path.join(folder, "work")
param_card = glob.glob(os.path.join(folder, "*._00001.tar*"))[0]
iseed = 22
file_num = len(glob.glob(os.path.join(folder, "*.tar*")))
# process = P.ttbarResonanceProcess.from_run_number(206781)

c.nevents = 1000000
while file_num < 200:
    print "Start from:", file_num + 1
    c.iseed = iseed
    main.fullsim(run_number, split_size = 12000, from_num = file_num + 1, param_card = param_card)
    # subprocess.Popen(["python", "main.py", "-i", str(iseed), "-s", "12000", "-n", "1000000", "--from_num", str(file_num + 1), "--param_card", param_card_from, "run_number", "206780"], stdout = sys.stdout, stdin = subprocess.PIPE).communicate()
    file_num = len(glob.glob(os.path.join(folder, "*.tar*")))
    iseed += 1
print "10M events Collected. Start RW!"
lhes = sorted(glob.glob(os.path.join(folder, "*.tar*")))[:200]
ntups = sorted(glob.glob(os.path.join(folder, "*.NTUP_TRUTH.root")))[:200]
assert len(lhes) == len(ntups) == 200
try:
    param_card_to = glob.glob("./run/output_20*to20*/*.{run_number_to}.*/*.tar*".format(run_number_to = run_number_to))[0]
    main.full_rw(lhes, ntups, run_number_to, param_card_to)
except:
    pass