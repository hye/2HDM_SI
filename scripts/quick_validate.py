#!/usr/bin/env python2.7
import Process, run_numbers
import glob, os
import config as c
import main
import warnings
import subprocess
template = """#!/bin/zsh
#$ -l h_vmem=4G
#$ -e {root_path}/shit.e.log
#$ -o {root_path}/shit.o.log

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

{root_path}/main.py {mediator} {mass} {tanb} {signal_type} -v
rm {template_file}
"""

for m, tanbs in zip((600, 700),([0.4, 0.5, 0.9, 1.5, 3.5, 6.0],[0.4, 0.5 , 0.7, 1.7, 3.0, 0.9, 1.5, 3.5])):
    for tanb in tanbs:
        for mediator in ("H","A"):
            for signal_type in ("S","SI"):
                process = Process.ttbarResonanceProcess(**{"m"+mediator: m,"tanb":tanb,"signal_type":signal_type})
                if (not glob.glob(os.path.join(c.output_path, str(process), "*.NTUP_REC.root"))):
                    n_tuptruths = len(glob.glob(os.path.join(c.output_path, str(process), "*.NTUP_TRUTH.root")))
                    n_lhe = len(glob.glob(os.path.join(c.output_path, str(process), "*.tar.gz")))
                    if n_tuptruths == n_lhe:
                        print process
                        print mediator, m, tanb, signal_type
                        template_file = os.path.join(c.work_path, str(process) + ".truthplots.sh")
                        with open(template_file, "w") as of:
                            of.write(template.format(root_path = c.root_path,mediator = mediator, mass = m, tanb = tanb, signal_type = signal_type, template_file = template_file))
                        with open(template_file, "r") as of:
                            print of.read()
                        subprocess.check_output(["qsub", template_file])
                        raise StopIteration
                    else:
                        warnings.warn("Process:\"%s\" not yet finished. (%s/%s)" % (process, n_tuptruths, n_lhe), RuntimeWarning)