include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "AFII MadGraph(CT10) Pythia Perugia 2011c gg->{mediator}->ttbar{interference}, singlelepton+dilepton, m({mediator})={mass}GeV, 2HDM Type2, tan(beta)={tanb:.2f}, sin(beta-alpha)=1.0, 8TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["gg{mediator}tt", "ttbar", "singlelepton", "dilepton" ]
evgenConfig.inputfilecheck = "madgraph.{run_number}.gg{mediator}tt"
evgenConfig.contact  = ["katharina.behr@cern.ch", "yu-heng.chen@cern.ch", "jike.wang@cern.ch"]
evgenConfig.minevents = 5000
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


